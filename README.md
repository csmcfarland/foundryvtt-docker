# Foundry VTT in Docker
I made this for myself to avoid having to manage a node environment.

## What does this do?
This will run Foundry VTT in a docker container, leaving the data folder as
a local volume that can be edited and managed outside of the container.

## Setup
The easiest way to use this is to clone the git repository and use the
`docker-compose` command to manage the container.

1. `git clone https://gitlab.com/csmcfarland/foundryvtt-docker.git`
1. `cd foundryvtt-docker`
1. `docker-compose up`
1. Browse to `localhost:30000`

## Modification
Once you have run the container, you will need to provide your own license key.
If you want to setup SSL keys, AWS integration, etc, refer to the
[Foundry VTT Installation Guide](https://foundryvtt.com/article/installation/).
