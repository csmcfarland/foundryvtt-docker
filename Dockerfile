FROM node:16

ARG version

ADD https://foundryvttreleases.s3.amazonaws.com/FoundryVTT-$version.zip ./foundry.zip
COPY entrypoint.sh /usr/local/bin/
RUN chmod u+x /usr/local/bin/entrypoint.sh && \
  mkdir -p /opt/foundryvtt && \
  mkdir -p /opt/foundrydata && \
  unzip -qq -d /opt/foundryvtt ./foundry.zip
ENTRYPOINT ["entrypoint.sh"]

WORKDIR /opt/foundryvtt
CMD ["node", "resources/app/main.js", "--dataPath=/opt/foundrydata", "--noupdate"]
