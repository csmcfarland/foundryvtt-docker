#!/usr/bin/env bash
set -e

echo "executing $@"
exec "$@"
